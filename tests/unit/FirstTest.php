<?php

use PHPUnit\Framework\TestCase;

class FirstTest extends TestCase
{
    /** @test */   // use this if function name not starts with test other use test before the function name
    public function multiplication_of_two_number(){   // method should start with test (like test_multiplication_of_two_number) (otherwise use above line)
        //$this->assertTrue(true); // will give (test 1 Assertion 1) as output
        //$this->assertTrue(false); // Will give (test 1 Assertion 1 failure 1  bcz it expects output true but false given)
        $a = 4;
        $b = 5;
        $c = $a * $b;
        $this->assertEquals($c,20); // $c equal to 20 means will return true 
        $this->assertEquals($c,30); // $c will give 20 but we have given 30 so it will display failure 1
    }
}